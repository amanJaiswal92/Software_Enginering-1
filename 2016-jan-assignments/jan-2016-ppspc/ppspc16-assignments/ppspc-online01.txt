Do not send response to this text before you read the entire text carefully.

Instructions:

1. You have to send a single file with extension "tar.gz" as attachment. No other extension will be processed further. Name of the attached file should be your roll number. Your roll number is a five digit string. If you do NOT name the file as per this rule, your assignment might be rejected.

2. You have to send an email to "assignments<DOT>pucsd<AT>gmail<DOT>com" with the subject:PPSPC16_ONLINE_01

Note that I am going to filter messages on the basis of the subject text ONLY. So, you should send the email with the exact subject text given above. Even slight change might result in rejection.
3. Deadline: 29/02/2016, 10PM

If you miss the deadline your assignment will be rejected. If you don't know the current time you may use this link [IST] to find the current time.

The assignment problem text starts here.
This is a group assignment.

You should write the following 3 programs

1. monitor
2. nextEven
3. nextOdd
4. ui
to accomplish the tasks discussed in the class.

The monitor will listen on port <jobsport> for incoming user requests.

You should submit a single file, with filename consisting of all group members' roll numbers as shown below. If the group contains 3 members then file name should be <rollno1-rollno2-rollno3.tar.gz>.
The assignment problem text ends here.

Do not send your assignment by replying to this mail without setting the subject text properly. The subject text to be used for this assignment is: PPSPC16_ONLINE_01
