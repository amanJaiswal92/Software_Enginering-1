Do not send response to this text before you read the entire text carefully.

Instructions:

1. You have to send a single file with extension "tar.gz" as attachment. No other extension will be processed further. Name of the attached file should be your roll number. Your roll number is a five digit string. If you do NOT name the file as per this rule, your assignment might be rejected.

2. You have to send an email to "assignments<DOT>pucsd<AT>gmail<DOT>com" with the subject:PPSPC16_GAME_02

Note that I am going to filter messages on the basis of the subject text ONLY. So, you should send the email with the exact subject text given above. Even slight change might result in rejection.
3. Deadline: 17/02/2016, 10AM

If you miss the deadline your assignment will be rejected. If you don't know the current time you may use this link [IST] to find the current time.

The assignment problem text starts here.
This is a group assignment.

Create a sample road input as discussed in the class and place some vehicles on the road.

Road is a polygon and and is covered by several quadrilaterals.

Vehicles are represented by rectangles and have colors.
You should create an image which has the following objects in it -

  - road which has at least 20 quadrilateral segments (see the example1.ps at [pslink] for an example quadrilateral. To view the file visually, you can use a program named evince; enter the command "evince example1.ps" on your machine.)
  - create 3 vehicle types as follows
  car   - with dimensions 3x5 square units
  truck - with dimensions 6x11 square units
  bike  - with dimensions 2x4 square units

  - create 4 cars, 2 trucks and 5 bikes to be shown as vehicles on the road
  
Write an XML text file which has the format as the one given in the file [road-format-sample.xml] and also write a ps file corresponding to that XML file.

You should submit a single file, with filename consisting of all group members' roll numbers as shown below. If the group contains 3 members then file name should be <rollno1-rollno2-rollno3.tar.gz>.
   
Refs-
http://www.physics.emory.edu/faculty/weeks//graphics/howtops1.html

http://partners.adobe.com/public/developer/ps/sdk/sample/index_psbooks.html

The assignment problem text ends here.

Do not send your assignment by replying to this mail without setting the subject text properly. The subject text to be used for this assignment is: PPSPC16_GAME_02
