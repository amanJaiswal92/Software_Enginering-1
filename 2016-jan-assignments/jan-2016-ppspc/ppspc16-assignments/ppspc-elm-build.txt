Do not send response to this text before you read the entire text carefully.

Instructions:

1. You have to send a single file with extension "tar.gz" as attachment. No other extension will be processed further. Name of the attached file should be your roll number. Your roll number is a five digit string. If you do NOT name the file as per this rule, your assignment might be rejected.

2. You have to send an email to "assignments<DOT>pucsd<AT>gmail<DOT>com" with the subject:
PPSPC_ELM_BUILD

Note that I am going to filter messages on the basis of the subject text ONLY. So, you should send the email with the exact subject text given above. Even slight change might result in rejection.
3. Deadline: 18/04/2016, 10PM

If you miss the deadline your assignment will be rejected. If you don't know the current time you may use this link [IST] to find the current time.

[IST] http://wwp.greenwichmeantime.com/time-zone/asia/india/time/


The assignment problem text starts here.

This is a group assignment.
Write a Python/Haskell script that will build from source, and install in the user's home directory, the following packages
 1. nodejs
 2. elm

You can assume that the system is a fresh install of Debian 7 or Debian 8, with build essentials installed. You should not assume any other packages installed.
Your script should not need network access during the build or install process. You may however keep the required package/source tarballs predownloaded in a specified directory. Your script should NOT require root previliges as much as possible.

Only one member from your group should submit a single file, with filename consisting of all group members' roll numbers as shown below. If the group contains 3 members then file name should be <rollno1-rollno2-rollno3.tar.gz>.

The assignment problem text ends here.

If you have any doubts, you may meet me.

Do not send your assignment by replying to this mail without setting the subject text properly. The subject text to be used for this assignment is:
PPSPC_ELM_BUILD
