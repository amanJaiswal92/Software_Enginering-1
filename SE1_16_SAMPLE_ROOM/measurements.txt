xMeasurements of all objects of Class Room :
------------------------------------------

(All measurements are in Centi-Meter)

object         |  width   |    length   |    height
----------------------------------------------------
Room           |   612    |   1360      |    318
----------------------------------------------------
beam           |   612    |   26        |    56    
----------------------------------------------------
Chalkbox       |   10     |   10        |    8
----------------------------------------------------
Amp Rack       |   50     |   60        |    76
----------------------------------------------------
BlackBox       |   49     |   52        |    59
----------------------------------------------------
Board          |   180    |   2         |    127
----------------------------------------------------
Switch Board 1 |   29     |   4         |    24 
----------------------------------------------------
Switch Board 2 |   15     |   4         |    15
----------------------------------------------------
Bench          |   90     |   75        |     72
-----------------------------------------------------
Table          |   150    |   75        |     75
-----------------------------------------------------
Door           |   105    |   3         |     205
-----------------------------------------------------
Stage          |   610    |   200       |     30
-----------------------------------------------------



Measure ment of Tile:
---------------------

   40*40 Cm^2

Tile which is visible through stage:
------------------------------------

	40*20 Cm^2 
