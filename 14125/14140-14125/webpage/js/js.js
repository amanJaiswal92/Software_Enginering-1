$(document).ready(function () {
$(document.body).attr('id',  'docBody');
$('#docBody').append($(document.createElement('div')).attr('id', 'prntDiv'));
$('#prntDiv').attr('class', 'container');
$('#docBody').append($(document.createElement('div')).attr('id', 'btnDiv'));
$('#btnDiv').attr('class', 'container');
$('#btnDiv').append($(document.createElement('button')).attr('id', 'btnPrint'));
$('#btnPrint').attr('value', 'PRINT');
$('#btnPrint').after().html('PRINT');
$('#btnDiv').append($(document.createElement('div')).attr('id', 'txtDiv'));
$('#txtDiv').attr('class', 'container');
$('#txtDiv').append($(document.createElement('textarea')).attr('id', 'txtBox'));
$('#txtBox').attr('rows', '10');
$('#txtBox').attr('cols', '100');
$('#btnPrint').on('click', '', function (event) {
var $test = $('body table tr.selected').find('td').map(function() {
return $(this).text();
}).get().join(', ');
$('#txtBox').text($test);

});
$('#prntDiv').append($(document.createElement('table')).attr('id', 'Unit_List'));
$('#Unit_List').attr('align', 'center');
$('#Unit_List').append($(document.createElement('tr')).attr('id', 'prntDivHeading'));
$('#prntDivHeading').append($(document.createElement('th')).attr('id', 'unit_id'));
$('#unit_id').after().html('unit_id');
$('#prntDivHeading').append($(document.createElement('th')).attr('id', 'unit_abbr'));
$('#unit_abbr').after().html('unit_abbr');
$('td').attr('contenteditable', 'true');
$('tr').attr('class', 'unselected');
$('body table').on('keydown', 'tr td', function (event) {
if (event.which == 39 ) {
event.preventDefault();
$(this).next().focus();
}

});
$('body table').on('keydown', 'tr td', function (event) {
if (event.which == 37 ) {
event.preventDefault();
$(this).prev().focus();
}

});
$('body table').on('keydown', 'tr td', function (event) {
if (event.which == 40 ) {
event.preventDefault();
$(this).parent().next().find('td')[$(this).index()].focus();
}

});
$('body table').on('keydown', 'tr td', function (event) {
if (event.which == 38 ) {
event.preventDefault();
$(this).parent().prev().find('td')[$(this).index()].focus();
}

});
$('body table').on('keydown', 'tr td', function (event) {
if (event.which == 114 ) {
event.preventDefault();
child = $(this).parent().parent().children($('tr.unselected'));
if (child.hasClass('unselected')) {
child.removeClass('unselected');
child.addClass('selected');
}
else {
child.removeClass('selected');
child.addClass('unselected');
}

}

});
$('body table').on('keydown', 'tr td', function (event) {
if (event.which == 115 ) {
event.preventDefault();
child = $(this).parent();
if (child.hasClass('unselected')) {
child.removeClass('unselected');
child.addClass('selected');
}
else {
child.removeClass('selected');
child.addClass('unselected');
}

}

});
$('body table').on('focusin', 'tr td', function (event) {
$(this).addClass('highlighted');
});
$('body table').on('focusout', 'tr td', function (event) {
$(this).removeClass('highlighted');
});
var nRowCount=0
$('#docBody').on('keypress', '', function (event) {
if (event.which == 13 ) {
event.preventDefault();
$('#Unit_List').append($(document.createElement('tr')).attr('id', 'row' + nRowCount));
$('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
$('td').attr('contenteditable', true);
$('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
$('td').attr('contenteditable', true);
$('tr').attr('class','unselected');
$('#row'+nRowCount+' td:first').focus();
nRowCount++

}

});

});