create table types(
    type_id text primary key,
    type_name text,
    description text,
);

create table objects(
       objectID text primary key,
       objectName text,
       type_id text references types(type_id),
);

create table typeInheritance (
    type_id integer references types(type_id),
    super_type_id integer references types(type_id),
    primary key(type_id, super_type_id),
);

create table containerConcreteTypes (
    containerConcreteTypeId text primary key references types(type_id),
    containerLengthAB integer, 
    containerLengthAD integer, 
    containerLengthAE integer, 
    containerThickness integer,
    userObjectCommentGlobal text,
);

create table containerConcreteTypeFaces (
    containerConcreteTypeId text references containerConcreteTypes (containerConcreteTypeId),
      roofID text ,
      roofName text,
      roofColor integer,
      rightID text,
      rightName text,
      rightColor integer,
      leftID text,
      leftName text,
      leftColor integer,
      floarID text,
      floarName text,
      floarColor integer,
      frontID text,
      frontName text,
      frontColor integer,
      backID text,
      backName text,
      backColor integer,
);

create table nonContainerConcreteTypeFaces (
    nonContainerConcreteTypeId text references nonContainerConcreteTypes (nonContainerConcreteTypeId),
      roofID text ,
      roofName text,
      roofColor integer,
      rightID text,
      rightName text,
      rightColor integer,
      leftID text,
      leftName text,
      leftColor integer,
      floarID text,
      floarName text,
      floarColor integer,
      frontID text,
      frontName text,
      frontColor integer,
      backID text,
      backName text,
      backColor integer,
);

create table nonContainerConcreteTypes (
    nonContainerConcrete_type_id text primary key references types(type_id),
    nonContainerLengthAB integer, 
    nonContainerLengthAD integer, 
    nonContainerLengthAE integer, 
    userObjectCommentGlobal text,
);

create table containerConcreteTypeProperties (
    containerConcreteTypeId text references containerConcreteTypes (containerConcreteTypeId),
    propertyID text references properties(propertyID), 
    propertyValue text, /* this will be the default property, can be overridden by making entry in objectProperties */
    measurementUnitID text references measurementUnits(measurementUnitID),  
    userCommentGlobal text,
        primary key(concrete_type_id, propertyID),
);

create table properties (
    propertyID text primary key, 
    propertyType text references propertyTypes(propertyTypes),
    userDescription text,
);

create table physicalEntities(
    physicalEntityID text primary key,
    entity_name text,
);

create table Unit_List (
    unit_id text primary key,
    unit_name text,
    unit_abbr text,
    physicalEntityID text references physicalEntities(physicalEntityID), 
    multiplication_factor float
);

create table propertyTypes (
    propertyType text, /* only from a list - date, string, bool, number, list of string, bool, number*/
    physicalEntityID text references physicalEntities(physicalEntityID), 
    userDescription text,
);

create table containerObjects (
    objectID text primary key,
    containerConcreteTypeId text references containerConcreteTypes (containerConcreteTypeId),
    userLabel text,
    userObjectCommentLocal text,
);

create table nonContainerObjects (
    objectID text primary key,
    nonContainerConcreteTypeId text references nonContainerConcreteTypes (nonContainerConcreteTypeId),
    userLabel text,
    userObjectCommentLocal text,
);

create table childObjectList(
       objectID text references containerObjects (objectID),
       childObjectID text primary key, 
       displacementAB integer,
       displacementAE integer,
       displacementAD integer,
);

create table nonContainerObjectProperties (
    objectID integer references objects (objectID),
    userAssignedID text NOT NULL,
    propertyID text references properties(propertyID), 
    propertyValue text NOT NULL,
    userCommentLocal text,
    userLabel text NOT NULL,
        primary key(objectID, propertyID)
);

create table containerTypeWallsDoor(
       doorID text,
       wallID text,
       containerConcreteTypeId text references containerConcreteTypes (containerConcreteTypeId),
       displacementAB integer,
       dispalcementAD integer,
       allignmentAB integer,
       dimAD integer,
       dimAB integer,
       allignmentAD text,
       allignementAB text,
       color integer,
);

create table compulsoryProperties ( 
       propertie text,
       objectID text references objects (objectID),
);

create table immutableProperties ( 
        objectID text references objects (objectID),	
);
