#!/usr/bin/python
import psycopg2
import sys
import ast
import psycopg2.extras
entry = []
lable = []
def TableAttribute(conn,tname):
    var = "SELECT * FROM " + tname
    cur = conn.cursor()
    cur.execute(var)
    for i in cur.description:
        entry.append(i[0])
    #print entry
   

def TableName(conn):
    cur = conn.cursor() 
    cur.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema='public' """)
    rows = cur.fetchall()
    for i in rows:
         entry.append(i[0])
    #print entry

def colRow(conn,tname,column):   
    cur = conn.cursor()

    sQuery = "SELECT "
    for i in range(len(column) - 1):
         sQuery += column[i] + ", ";
    sQuery += column[len(column)-1] + " "
    sQuery += "FROM " + tname
    cur.execute(sQuery)
    lRows = cur.fetchall();
    for i in lRows:
        for j in i:
            entry.append(j)
    print entry

def connect(name,connection,ls):
    try:
        conn = psycopg2.connect(connection)
        #print "Connecting to database\n->%s" % (conn)
        if (len(ls)==0):
            if name == "database":
                TableName(conn)
            else:
                TableAttribute(conn,name)
        else:
            colRow(conn,name,ls)
    #conn.commit()        
    except psycopg2.DatabaseError, e:
        print 'Error %s' % e    
        sys.exit(1)
    # finally:
    #     if conn:
    #         conn.close()
    

def Html(row,col,ls,label):
    f = open('ui.html','w')
    headder = """ <!DOCTYPE html> \n <html>\n <head></head>\n <body>\n <table id="table" cellspacing="0" border="1"> """
    f.write(headder)
    msg = "<tr><td>\n"
    for i in range(0,col):
        msg = msg + "<input type = 'text' value='"+label[i]+"' readonly/>&nbsp;\n"
    msg = msg + "</td></tr>\n"
    f.write(msg)
    k = 1
    msg = ""
    for i in range(1,row+1):
         var = ""
         for j in range(1,col+1):
             var = var + "<input type = 'text' id="+"\""+str(k)+"\""+" value = '" + str(ls[k-1]) +"' />&nbsp;"+"\n"
             k=k+1
         var = var + "<input type = \"checkbox\" name=\"chckbox\"  id=\"cb"+str(i)+"\""+" onchange=\"CopyData();\" />"+"\n"
         msg =msg+ "<tr> \n <td> \n" + var + "</td>\n </tr>"+"\n"
   
    msg = msg+ """ <tr>
                <td style="text-align:center">
                    <input type='text' id="textval" style="height:60px" />
                </td>
            </tr>\n""" + "<script>"

    msg = msg + "function CopyData()\n{\n"
    k=1
    for i in range(1,row+1):
        for j in range(1,col+1):
            msg = msg + "var val"+str(k)+" = document.getElementById('"+str(k)+"')\n"
            k=k+1
        msg = msg + "var cb"+str(i)+" = document.getElementById('cb"+str(i)+"')\n"
    msg = msg + "var textdat = document.getElementById('textval')\n"
    k=1    
    for i in range(1,row+1):
        msg = msg + "if(cb"+str(i)+".checked )\n textdat.value = textdat.value + ',' "
        for j in range(1,col+1):
           msg = msg + "+ val"+str(k)+".value + ','"
           k=k+1
        msg = msg + "\n"
    msg = msg + "}\nvar curr_i = 1;\n var min_i = 1;\n var max_i = "+str(row*col)+";\n document.getElementById(curr_i).focus()\n  window.addEventListener(\"keydown\",keyEvent,false)\n function keyEvent(e)\n { \n switch(e.keyCode)\n {\n case 114:\n"
    
    var = "textdat.value = "
    for i in range(1,row*col+1):
        msg = msg + "var val"+str(i)+" = document.getElementById('"+str(i)+"')\n"
        var = var + "val"+str(i)+".value + ',' + "
    msg = msg + "var textdat = document.getElementById('textval')\n var cb = document.getElementsByName('chckbox')\n for(var i = 0; i < cb.length; i++)\n { \n cb[i].checked = true;\n }\n"
    msg = msg + var + "','\n break;\n"
    msg = msg + " case 38: \n curr_i = curr_i - 2 \n document.getElementById(curr_i).focus();\n  break;\n case 40:\n curr_i = curr_i + 2\n document.getElementById(curr_i).focus()\n break;\n case 39:\n curr_i = curr_i + 1 \n document.getElementById(curr_i).focus() \n break;\n case 37:\n curr_i = curr_i - 1 \n document.getElementById(curr_i).focus() \n break;\n }\n } \n            </script>\n         </table>\n    </body>\n</html>\n"
    f.write(msg)


    f.close()

if __name__=='__main__':
    ls = ast.literal_eval(sys.argv[3])
    if(len(ls)==0):
        connect(sys.argv[1],sys.argv[4],ls)
        if sys.argv[1]=="database":
            label = ["Database Tables"]
            Html(len(entry),1,entry,label)
        else:
            label = ["Table Names"]
            Html(len(entry),1,entry,label)
    else:
        label = ls
        connect(sys.argv[1],sys.argv[4],ls)
        Html(len(entry)/len(ls),len(ls),entry,label)
   
   
