create table types (
    typeId      text primary key,
    typeName   text NOT NULL,
    description text
);

create table typeInheritance (
    typeId       text references types(typeId),
    superTypeId text references types(typeId),
    primary key(typeId, superTypeId),
    check (typeId != superTypeId )
);

create table containerConcreteTypes (
    containerConcreteTypeID text primary key references types (typeId),
    containerLengthAB       integer NOT NULL,
    containerLengthAD       integer NOT NULL,
    containerLengthAE       integer NOT NULL,
    containerThickness      integer NOT NULL,
    userObjectCommentGlobal text
);

create table boxWalls (
    boxWallSideName text primary key /*It is one of the 6 walls i.e. Front, Back, Roof, Floor, Right, Left*/
);

create table validOrientations (
    boxValidWallOrientations text primary key /*It is one of the ABCD, BCDA, CDAB, DABC, DCBA, CBAD, BADC,  ADCB */
);

create table containerConcreteTypeFaces (
    containerConcreteWallID text PRIMARY KEY,
    containerConcreteTypeID text references containerConcreteTypes(containerConcreteTypeID),
    containerSides          text references boxWalls(boxWallSideName),
    wallOrientation         text references validOrientations(boxValidWallOrientations)
);

create table containerConcreteTypeFaceDoors (
    containerConcreteDoorID text PRIMARY KEY,
    containerConcreteWallID text references containerConcreteTypeFaces(containerConcreteWallID),
    doorWidthAB             integer NOT NULL,
    doorLengthAD            integer NOT NULL,
    doorDisplacementAB      integer NOT NULL,
    doorDisplacementAD      integer NOT NULL,
    doorAlignmentAB         text NOT NULL,
    doorAlignmentAD         text NOT NULL
);


create table objectTypes (
    objectType text PRIMARY KEY
    /* i.e. container or nonContainer */
);

create table objects (
    objectID   text PRIMARY KEY,
    objectType text references objectTypes(objectType)
    
);

create table physicalEntities (
    physicalEntityID text primary key,
    entityName      text NOT NULL
);

create table propertyTypes (
    propertyType     text primary key, /* only from a list - date, string, bool, number, list of string, bool, number*/
    physicalEntityID text references physicalEntities(physicalEntityID), 
    userDescription  text
);



create table properties (
    propertyID      text primary key,
    propertyType    text references propertyTypes(propertyType), -- references propertyTypes(propertyType)
    userDescription text
);

create table Unit_List (
    unitId               text primary key,
    unitName             text,
    unitAbbr             text,
    physicalEntityID      text references physicalEntities(physicalEntityID), 
    multiplication_factor float
);

create table measurementUnits (
       measurementUnitId   text primary key,
       measurementUnitType text references Unit_List(unitId)
);

create table containerConcreteTypeProperties (
    containerConcreteTypeID text references containerConcreteTypes (containerConcreteTypeID),
    propertyID              text references properties(propertyID),
    propertyValue           text,
    measurementUnits        text references measurementUnits(measurementUnitID),
    userCommentGlobal       text,
    primary key(containerConcreteTypeID, propertyID)
);

create table containerObjects (
    objectID                text references objects(objectID),
    containerConcreteTypeId text references containerConcreteTypes (containerConcreteTypeId),
    userLabel               text,
    userObjectCommentLocal  text,
    primary key(objectID)
);

create table nonContainerConcreteTypes (
    nonContainerConcreteTypeID text primary key references types (typeId),
    nonContainerLengthAB       integer NOT NULL,
    nonContainerLengthAD       integer NOT NULL,
    nonContainerLengthAE       integer NOT NULL,
    userObjectCommentGlobal text
);

create table nonContainerObjects (
    objectID                   text references objects(objectID),
    nonContainerConcreteTypeId text references nonContainerConcreteTypes (nonContainerConcreteTypeId),
    userLabel                  text,
    userObjectCommentLocal     text,
    primary key(objectID)
);

create table nonContainerObjectProperties (
    objectID         text references nonContainerObjects (objectID),
    userAssignedID   text NOT NULL,
    propertyID       text references properties(propertyID), 
    propertyValue    text NOT NULL,
    userCommentLocal text,
    userLabel        text NOT NULL,
    primary key(objectID, propertyID)
);

create table childrenList (
    parentObjectID         text references containerObjects(objectID),
    objectID               text references objects(objectID),
    displacementAB         integer NOT NULL,
    displacementAD         integer NOT NULL,
    displacementAE         integer NOT NULL,
    childFrontFacingParent text    NOT NULL,
    childTopFacingParent   text    NOT NULL
);

create table compulsoryProperties ( 
       propertyID text references properties (propertyID)
    /*If property is there it is compulsory else its not.*/
 );

create table immutableProperties ( 
    propertyID text references properties (propertyID)
    /*If property is there it is immutable else its not.*/
);



/*Insertion into tables for validations*/

insert into validOrientations values('ABCD'), ('BCDA'), ('CDAB'), ('DABC'), ('DCBA'), ('CBAD'), ('BADC'), ('ADCB');
insert into validOrientations values('abcd'), ('bcda'), ('cdab'), ('dabc'), ('dcba'), ('cbad'), ('badc'), ('adcb');
insert into boxwalls values('FRONT'),('BACK'),('ROOF'),('FLOOR'),('LEFT'),('RIGHT');

--trigger code

CREATE FUNCTION chkAllVals() RETURNS trigger AS $chkVals$
BEGIN
	IF (SELECT COUNT(*) FROM typeInheritance WHERE typeId=NEW.super_typeId AND super_typeId=NEW.typeId) > 0 THEN
	RAISE EXCEPTION 'Error: Can not create super type of the sub type.';
	END IF;
	RETURN NEW;
END;
$chkVals$ language plpgsql;

CREATE TRIGGER chkVals BEFORE INSERT ON typeInheritance FOR EACH ROW EXECUTE PROCEDURE chkAllVals();


--Drop table queries;
drop table immutableProperties ; 
drop table compulsoryProperties; 
drop table childrenList;
drop table nonContainerObjectProperties;
drop table nonContainerObjects;
drop table nonContainerConcreteTypes;
drop table containerObjects;
drop table containerConcreteTypeProperties;
drop table measurementUnits;
drop table Unit_List;
drop table properties;
drop table propertyTypes;
drop table physicalEntities;
drop table objects;
drop table objectTypes;
drop table containerConcreteTypeFaceDoors;
drop table containerConcreteTypeFaces;
drop table validOrientations;
drop table boxWalls;
drop table containerConcreteTypes;
drop table typeInheritance;
drop table types;
