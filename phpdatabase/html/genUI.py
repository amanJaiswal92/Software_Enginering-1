# python genUI.py <table_name> <nRows> "[<column Names>]" "<db connection string>"

import sys, getopt
import JsGenFunctions
import ast
import psycopg2

sParentDivID  = "prntDiv"

sBodyID = "docBody"

def xstr(s):
   if s is None:
      return ''
   return str(s)

def main(argv):
   # Connect to Databse
   # conn = psycopg2.connect("dbname='objdb' user='postgres' host='localhost' password='horcrux'")
   try:
      conn = psycopg2.connect(argv[3])
   except:
      print "Error: Database Connection Error."
      sys.exit(0)



   nRows = int(argv[1])
   lColumns        = ast.literal_eval(argv[2])

   # Get the list of Columns from the database:

   curDB = conn.cursor()
   lRows  = []
   try:
      sSqlQuery = "SELECT "
      for nI in range(len(lColumns) - 1):
         sSqlQuery += xstr(lColumns[nI]) + ", ";
      sSqlQuery += xstr(lColumns[len(lColumns)-1]) + " ";
      sSqlQuery += "FROM " + argv[0]
      curDB.execute(sSqlQuery)
      lRows = curDB.fetchall();

   except:
      print "Error: No Columns found in the Tables.\n"
      sys.exit(0)


   # Give body a id. For generalization.
   sBodyIDHtml     = JsGenFunctions.fAddBodyAttrJs(sBodyID)
   sBodyIDHtml    += JsGenFunctions.fGenerateJSObj("div", [('class', 'container')], sBodyID, sParentDivID)

   sTableId = argv[0]

   # Generate a Table Object.
   sTableHtml      = (xstr(JsGenFunctions.fGenerateJSObj("table", [('align', 'center')], sParentDivID, sTableId)))

   sTableHeadingId = sParentDivID + "Heading"
   # Genearte the Table Headings.
   sTableHeadHTML  = xstr(JsGenFunctions.fGenerateJSObj("tr", [], sTableId, sTableHeadingId))
   for column in lColumns:
      sTableHeadHTML += xstr(JsGenFunctions.fGenerateJSObj("th", [], sTableHeadingId, column))
      sTableHeadHTML += xstr(JsGenFunctions.fGenerateJSHTML(column, str(column)))

   # Generate the Table rows.
   sTableRowsHTML  = ""
   nRowCount = 0
   for nI in range(len(lRows)):
      sTableRowID     = sTableId + "Row" + str(nI)
      sTableRowsHTML += xstr(JsGenFunctions.fGenerateJSObj("tr", [], sTableId, sTableRowID))
      nRowCount+=1
      for nJ in range(len(lRows[nI])):
         sTableCellID = lColumns[nJ] + "Row" + str(nI)
         sTableRowsHTML += xstr(JsGenFunctions.fGenerateJSObj("td", [], sTableRowID, sTableCellID))
         sTableRowsHTML += xstr(JsGenFunctions.fGenerateJSHTML(sTableCellID, str(lRows[nI][nJ])))



   # print sTableRowsHTML
   sAttr  = xstr(JsGenFunctions.fApplyAttributeTag("td", ("contenteditable" , "true")));
   sAttr += xstr(JsGenFunctions.fApplyAttributeTag("tr", ("class" , "unselected")));
   sSomeOut = xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("keydown", "39" ,"event.preventDefault();\n$(this).next().focus();"), True, "tr td"))
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("keydown", "37" ,"event.preventDefault();\n$(this).prev().focus();"), True, "tr td"))
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("keydown", "40" ,"event.preventDefault();\n$(this).parent().next().find('td')[$(this).index()].focus();"), True, "tr td"))
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("keydown", "38" ,"event.preventDefault();\n$(this).parent().prev().find('td')[$(this).index()].focus();"), True, "tr td"))
   sF3Defi   = "event.preventDefault();\nchild = $(this).parent().parent().children($('tr.unselected'));\nif (child.hasClass('unselected')) {\nchild.removeClass('unselected');\nchild.addClass('selected');\n}\nelse {\nchild.removeClass('selected');\nchild.addClass('unselected');\n}\n"
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("keydown", "114" , sF3Defi), True, "tr td"))
   sF4Defi   = "event.preventDefault();\nchild = $(this).parent();\nif (child.hasClass('unselected')) {\nchild.removeClass('unselected');\nchild.addClass('selected');\n}\nelse {\nchild.removeClass('selected');\nchild.addClass('unselected');\n}\n"
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("keydown", "115" , sF4Defi), True, "tr td"))
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("focusin", "$(this).addClass('highlighted');"), True, "tr td"))
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler("body table", ("focusout", "$(this).removeClass('highlighted');"), True, "tr td"))
   sSomeOut += "var nRowCount="+xstr(nRowCount)+"\n"
   sCells    = "$('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))\n$('td').attr('contenteditable', true);\n"
   sCellsHTML = ""
   for nJ in range(len(lColumns)):
      sCellsHTML += sCells;
   sSomeOut += xstr(JsGenFunctions.fGenerateJSEventHandler(sBodyID, ("keypress", "13", "event.preventDefault();\n$('#" + sTableId + "').append($(document.createElement('tr')).attr('id', 'row' + nRowCount));\n" + sCellsHTML + "$('tr').attr('class','unselected');\n$('#row'+nRowCount+' td:first').focus();\n" +"nRowCount++\n"), False, ""))


   # Generate button HTML JS
   sBtnHTML  = xstr(JsGenFunctions.fGenerateJSObj("div", [('class', 'container')], sBodyID, "btnDiv"))
   sBtnHTML +=xstr(JsGenFunctions.fGenerateJSObj("button", [('value', 'PRINT')], "btnDiv", "btnPrint"))
   sBtnHTML += xstr(JsGenFunctions.fGenerateJSHTML("btnPrint", "PRINT"));
   sBtnHTML += xstr(JsGenFunctions.fGenerateJSObj("div", [('class', 'container')], "btnDiv", "txtDiv"))
   sBtnHTML += xstr(JsGenFunctions.fGenerateJSObj("textarea", [('rows', '10'), ('cols', '100')], "txtDiv", "txtBox"))
   sBtnJs    = xstr(JsGenFunctions.fGenerateJSEventHandler('btnPrint', ("click", "var $test = $('body table tr.selected').find('td').map(function() {\nreturn $(this).text();\n}).get().join(', ');\n$('#txtBox').text($test);\n"), False, ""))
   sFinalOut = "$(document).ready(function () {\n" + sBodyIDHtml + sBtnHTML + sBtnJs + sTableHtml + sTableHeadHTML + sTableRowsHTML + sAttr + sSomeOut + "\n});"
   f = open('webpage/js/js.js', 'w')
   f.write(sFinalOut)
   f.close()




   sHtmlFileData = "<html>\n<head>\n <link rel='stylesheet' href='css/style.css' />\n<script src='js/jquery.js'></script>\n<script src='js/js.js'></script>\n</head>\n<body>\n</body>\n</html>"
   f = open('webpage/homepage.html', 'w')
   f.write(sHtmlFileData)
   f.close()

if __name__ == "__main__":
   main(sys.argv[1:])
