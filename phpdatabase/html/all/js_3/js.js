$(document).ready(function () {
    var editingSession = false;
    $(document.body).attr('id',  'docBody');
    $('#docBody').append($(document.createElement('div')).attr('id', 'prntDiv'));
    $('#prntDiv').attr('class', 'container');
    $('#docBody').append($(document.createElement('div')).attr('id', 'btnDiv'));
    $('#btnDiv').attr('class', 'container');
    $('#btnDiv').append($(document.createElement('button')).attr('id', 'btnPrint'));
    $('#btnPrint').attr('value', 'PRINT');
    $('#btnPrint').after().html('PRINT');
    $('#btnDiv').append($(document.createElement('div')).attr('id', 'txtDiv'));
    $('#txtDiv').attr('class', 'container');
    $('#txtDiv').append($(document.createElement('textarea')).attr('id', 'txtBox'));
    $('#txtBox').attr('rows', '10');
    $('#txtBox').attr('cols', '100');
    $('#btnPrint').on('click', '', function (event) {
	var $test = $('body table tr.selected').find('td').map(function() {
	    return $(this).text();
	}).get().join(', ');
	$('#txtBox').text($test);

    });
    $('#prntDiv').append($(document.createElement('table')).attr('id', 'searchTable'));
    $('#searchTable').attr('align', 'center');
    $('#searchTable').attr('tabindex', '1');
    $('#searchTable').append($(document.createElement('tr')).attr('id', 'search-container-row'));
    $('#search-container-row').append($(document.createElement('td')).attr('id', 'searchBox0'));
    $('#searchBox0').attr('contenteditable', 'false');
    $('#searchBox0').attr('class', 'search');
    $('#search-container-row').append($(document.createElement('td')).attr('id', 'searchBox1'));
    $('#searchBox1').attr('contenteditable', 'false');
    $('#searchBox1').attr('class', 'search');
    $('#prntDiv').append($(document.createElement('table')).attr('id', 'Unit_List'));
    $('#Unit_List').attr('align', 'center');
    $('#Unit_List').attr('tabindex', '0');
    $('#Unit_List').append($(document.createElement('tr')).attr('id', 'prntDivHeading'));
    $('#prntDivHeading').append($(document.createElement('th')).attr('id', 'unit_id'));
    $('#unit_id').after().html('unit_id');
    $('#prntDivHeading').append($(document.createElement('th')).attr('id', 'unit_abbr'));
    $('#unit_abbr').after().html('unit_abbr');
    $('tr').attr('class', 'unselected');
    var newDone = false;
    var newRow = false;
    var editingSession = false;
    var displayMode = {false:'none', true:''};
    var highlightedCell;
    function goUp(current) {
    var obj = current.parent().prev().children('td:nth-child(' + (current.index() + 1) + ')');    
    if(obj. length ==0)
        return;
    if(newRow)
        newDone = true;
    if(!newRowFun())
        return;
	return obj;
        
    }
    function goDown ( current) {
    var obj = current.parent().next().children('td:nth-child(' + (current.index() + 1) + ')');
    if(obj.length ==0)
        return;
    if(newRow)
        newDone = true;
    if(!newRowFun())
        return;
	return obj;
    }
    function goLeft ( current) {
	return current.prev('td');
    }
    function goRight ( current) {
	return current.next('td');
    }
    function focusThis ( object) {
	object.attr('tabindex', '0');
	object.focus();
    }
    var updations = [];
    var upi = 0;
    var firstVal;
    var curHigh;
    var secondVal;
    function stopEditing ( current) {
    if(!editingSession)
        return;
    if($('.highlighted').parent().hasClass('editing') && ! newRow && !$('.highlighted').hasClass('search')) {
        alert('update');
    }
    $('.highlighted').parent().removeClass('editing');
	current.attr('contentEditable', 'false');
	$('.borderColor').toggleClass('borderColor');
	editingSession = false;
  //      secondVal = curHigh.parent();
  //  updations.push = ([firstVal, secondVal]);
//    alert(updations[upi][0].find('td').eq(0).text());
    upi++;
    }
    $('table').on('keydown', '', function (event) {
	if (event.which == 39 || event.which == 38 || event.which == 37 || event.which == 40) {
	    if(editingSession) {return;}
	    event.preventDefault();
	    var current   = $('.highlighted');
	    var xs = {39:goRight, 37:goLeft, 40:goDown, 38:goUp };
	    var nextChild = xs[event.which](current);
	    var tempCurrent;
	    while(nextChild.parent().css('display') == 'none' && nextChild.length > 0) {
		tempCurrent = nextChild;
		nextChild = xs[event.which](tempCurrent);
	    }
	    if(nextChild.length == 0) {
		return;
	    }
	    current.removeClass('highlighted');
	    nextChild.addClass('highlighted');

	}

    });
    $('#Unit_List').on('keydown', '', function (event) {
	if (event.which == 83 ) {
	    if (event.ctrlKey || event.metaKey) {
		switch (String.fromCharCode(event.which).toLowerCase()) {
		case 's':
		    event.preventDefault();
            if(newRow)
                newDone = true;
            if(!newRowFun())
                return;
		    highlightedCell = $('.highlighted');
		    highlightedCell.toggleClass('highlighted');
		    stopEditing(highlightedCell);
		    $('#searchTable tr:nth-child(1)').find('td').eq(highlightedCell.index()).toggleClass('highlighted');
		    $('#searchTable').focus();
		    break;
		}
	    }

	}

    });
    $('#searchTable').on('keydown', '', function (event) {
	if (event.which == 71 || event.which == 67) {
	    if (event.ctrlKey || event.metaKey) {
		switch (String.fromCharCode(event.which).toLowerCase()) {
		case 'g':
		    event.preventDefault();
		    var ups = highlightedCell;
		    while(ups.parent().css('display') == 'none' && ups.length>0) {
			ups = goUp(ups);
		    }
		    if(ups.length == 0) {
			ups = highlightedCell;
		    }
            while(ups.parent().css('display') == 'none' && ups.length>0) {
			ups = goDown(ups);
		    }
                
            if(ups.length > 0) {
                stopEditing($('.highlighted'));
                $('.highlighted').blur();
		      $('.highlighted').toggleClass('highlighted');
		      highlightedCell = ups;
		      highlightedCell.toggleClass('highlighted');
		      highlightedCell.parent().parent().parent().focus();
                
            }
            break;
		case 'c':
		    event.preventDefault();
		    $('.highlighted').after().html('');
		    break;
		}
	    }

	}

    });
    $('table').on('keydown', '', function (event) {
	if (event.which == 114 ) {
	    event.preventDefault();
	    stopEditing($('.highlighted'));
	    child = $('.highlighted').parent().parent().children($('tr.unselected'));
	    child.each(function () {
		if($(this).css('display') != 'none') {
		    $(this).toggleClass('unselected');
		    $(this).toggleClass('selected');
		}
	    });

	}

    });
   
    function startEditing ( ) {
        $('.highlighted').parent().addClass('editing');

	$('.highlighted').attr('contentEditable', 'true');
	$('.highlighted').addClass('borderColor');
	editingSession = true;
	$('.highlighted').focus();
    }
    $('table').on('keydown', function (event) {
	if(event.which >= 65 && event.which <= 90 && !event.ctrlKey) {
	    startEditing();
	}
    });
    $('table').on('keydown', '', function (event) {
	if (event.which == 45 ) {
	    event.preventDefault();
	    startEditing();
	}

    });
    
    
    
    
    $('table').on('keydown', '', function (event) {
	if (event.which == 35 ) {
	    event.preventDefault();
	    $('.highlighted').parent().parent().parent().focus();
	    stopEditing($('.highlighted'));
	}

    });
    $('table').on('keydown', '', function (event) {
	if (event.which == 115 ) {
	    event.preventDefault();
	    child = $('.highlighted').parent();
	    child.toggleClass('unselected');
	    child.toggleClass('selected');

	}

    });
    var nRowCount=0
    
    var tableName = 'Unit_List';
    function newRowFun() {
        if(!newDone) { return true; }
        newRow = false;
        newDone = false;
       
        return true;
    }
    $('#Unit_List').on('keypress', '', function (event) {
	if (event.which == 13 ) {
        allTrs = $(this).find('tr');
        var fl = false;
        if(allTrs.length > 1) {
            allTrs.eq(allTrs.length-1).find('td').each(function() {
                if($(this).text() != '') {
                    fl = true;
                }
            });
        }
        else
            fl = true;
        if(fl) {
	       event.preventDefault();
            if(newRow)
                newDone = true;
            if(!newRowFun())
                return;
	       $('#Unit_List').append($(document.createElement('tr')).attr('id', 'row' + nRowCount));
	       $('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
	       $('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
	       $('#row' + nRowCount).attr('class','unselected');
	       $('#row'+nRowCount+' td:first').focus();
            
           stopEditing($('.highlighted'));
            $('#row' + nRowCount).addClass('new');
            newRow = true;
	       $('.highlighted').toggleClass('highlighted');
	       $('#row'+nRowCount+' td:first').toggleClass('highlighted');
	       nRowCount++;
        }

    }  

    });
    
    $('#searchTable').on('keyup', '', function (event) {
	var nCols = $(this).parent().find('td').length;
	var i;
	$('#Unit_List tr').each(function() {
	    var flag = true;
	    for(i = 0; i < nCols; ++i) {
		var textValue = (($('#searchTable tr:nth-child(1)').find('td').eq(i).text()));
		if($(this).find('td').eq(i).text().toLowerCase().search(textValue.toLowerCase()) < 0 && $(this).find('td').length > 0) {
		    flag = false;
		    break;
		}
	    }
	    $(this).css('display', displayMode[flag]);
	});

    });

    $('#Unit_List').focus();
    $('#Unit_List').children('tr:nth-child(1)').children('td:nth-child(1)').toggleClass('highlighted');
});
