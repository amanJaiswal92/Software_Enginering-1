$(document).ready(function () {
var editingSession = false;
$(document.body).attr('id',  'docBody');
$('#docBody').append($(document.createElement('div')).attr('id', 'prntDiv'));
$('#prntDiv').attr('class', 'container');
$('#docBody').append($(document.createElement('div')).attr('id', 'btnDiv'));
$('#btnDiv').attr('class', 'container');
$('#btnDiv').append($(document.createElement('button')).attr('id', 'btnPrint'));
$('#btnPrint').attr('value', 'PRINT');
$('#btnPrint').after().html('PRINT');
$('#btnDiv').append($(document.createElement('div')).attr('id', 'txtDiv'));
$('#txtDiv').attr('class', 'container');
$('#txtDiv').append($(document.createElement('textarea')).attr('id', 'txtBox'));
$('#txtBox').attr('rows', '10');
$('#txtBox').attr('cols', '100');
$('#btnPrint').on('click', '', function (event) {
var $test = $('body table tr.selected').find('td').map(function() {
return $(this).text();
}).get().join(', ');
$('#txtBox').text($test);

});
$('#prntDiv').append($(document.createElement('table')).attr('id', 'searchTable'));
$('#searchTable').attr('align', 'center');
$('#searchTable').attr('tabindex', '1');
$('#searchTable').append($(document.createElement('tr')).attr('id', 'search-container-row'));
$('#search-container-row').append($(document.createElement('td')).attr('id', 'searchBox0'));
$('#searchBox0').attr('contenteditable', 'false');
$('#searchBox0').attr('class', 'search');
$('#search-container-row').append($(document.createElement('td')).attr('id', 'searchBox1'));
$('#searchBox1').attr('contenteditable', 'false');
$('#searchBox1').attr('class', 'search');
$('#search-container-row').append($(document.createElement('td')).attr('id', 'searchBox2'));
$('#searchBox2').attr('contenteditable', 'false');
$('#searchBox2').attr('class', 'search');
$('#prntDiv').append($(document.createElement('table')).attr('id', 'types'));
$('#types').attr('align', 'center');
$('#types').attr('tabindex', '0');
$('#types').append($(document.createElement('tr')).attr('id', 'prntDivHeading'));
$('#prntDivHeading').append($(document.createElement('th')).attr('id', 'typeid'));
$('#typeid').after().html('typeid');
$('#prntDivHeading').append($(document.createElement('th')).attr('id', 'type_name'));
$('#type_name').after().html('type_name');
$('#prntDivHeading').append($(document.createElement('th')).attr('id', 'description'));
$('#description').after().html('description');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow0'));
$('#typesRow0').append($(document.createElement('td')).attr('id', 'typeidRow0'));
$('#typeidRow0').after().html('x');
$('#typesRow0').append($(document.createElement('td')).attr('id', 'type_nameRow0'));
$('#type_nameRow0').after().html('y');
$('#typesRow0').append($(document.createElement('td')).attr('id', 'descriptionRow0'));
$('#descriptionRow0').after().html('z');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow1'));
$('#typesRow1').append($(document.createElement('td')).attr('id', 'typeidRow1'));
$('#typeidRow1').after().html('a');
$('#typesRow1').append($(document.createElement('td')).attr('id', 'type_nameRow1'));
$('#type_nameRow1').after().html('ssd');
$('#typesRow1').append($(document.createElement('td')).attr('id', 'descriptionRow1'));
$('#descriptionRow1').after().html('ere');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow2'));
$('#typesRow2').append($(document.createElement('td')).attr('id', 'typeidRow2'));
$('#typeidRow2').after().html('e');
$('#typesRow2').append($(document.createElement('td')).attr('id', 'type_nameRow2'));
$('#type_nameRow2').after().html('f');
$('#typesRow2').append($(document.createElement('td')).attr('id', 'descriptionRow2'));
$('#descriptionRow2').after().html('g');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow3'));
$('#typesRow3').append($(document.createElement('td')).attr('id', 'typeidRow3'));
$('#typeidRow3').after().html('l');
$('#typesRow3').append($(document.createElement('td')).attr('id', 'type_nameRow3'));
$('#type_nameRow3').after().html('x');
$('#typesRow3').append($(document.createElement('td')).attr('id', 'descriptionRow3'));
$('#descriptionRow3').after().html('z');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow4'));
$('#typesRow4').append($(document.createElement('td')).attr('id', 'typeidRow4'));
$('#typeidRow4').after().html('eaa');
$('#typesRow4').append($(document.createElement('td')).attr('id', 'type_nameRow4'));
$('#type_nameRow4').after().html('eeeb');
$('#typesRow4').append($(document.createElement('td')).attr('id', 'descriptionRow4'));
$('#descriptionRow4').after().html('dafc');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow5'));
$('#typesRow5').append($(document.createElement('td')).attr('id', 'typeidRow5'));
$('#typeidRow5').after().html('safsdafasdfsdaf');
$('#typesRow5').append($(document.createElement('td')).attr('id', 'type_nameRow5'));
$('#type_nameRow5').after().html('asdfasdf');
$('#typesRow5').append($(document.createElement('td')).attr('id', 'descriptionRow5'));
$('#descriptionRow5').after().html('asdfsadf');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow6'));
$('#typesRow6').append($(document.createElement('td')).attr('id', 'typeidRow6'));
$('#typeidRow6').after().html('asdererere');
$('#typesRow6').append($(document.createElement('td')).attr('id', 'type_nameRow6'));
$('#type_nameRow6').after().html('ererererer');
$('#typesRow6').append($(document.createElement('td')).attr('id', 'descriptionRow6'));
$('#descriptionRow6').after().html('ererer');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow7'));
$('#typesRow7').append($(document.createElement('td')).attr('id', 'typeidRow7'));
$('#typeidRow7').after().html('lalit');
$('#typesRow7').append($(document.createElement('td')).attr('id', 'type_nameRow7'));
$('#type_nameRow7').after().html('kumar');
$('#typesRow7').append($(document.createElement('td')).attr('id', 'descriptionRow7'));
$('#descriptionRow7').after().html('nankani');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow8'));
$('#typesRow8').append($(document.createElement('td')).attr('id', 'typeidRow8'));
$('#typeidRow8').after().html('iop');
$('#typesRow8').append($(document.createElement('td')).attr('id', 'type_nameRow8'));
$('#type_nameRow8').after().html('pop');
$('#typesRow8').append($(document.createElement('td')).attr('id', 'descriptionRow8'));
$('#descriptionRow8').after().html('lop');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow9'));
$('#typesRow9').append($(document.createElement('td')).attr('id', 'typeidRow9'));
$('#typeidRow9').after().html('d');
$('#typesRow9').append($(document.createElement('td')).attr('id', 'type_nameRow9'));
$('#type_nameRow9').after().html('');
$('#typesRow9').append($(document.createElement('td')).attr('id', 'descriptionRow9'));
$('#descriptionRow9').after().html('');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow10'));
$('#typesRow10').append($(document.createElement('td')).attr('id', 'typeidRow10'));
$('#typeidRow10').after().html('This');
$('#typesRow10').append($(document.createElement('td')).attr('id', 'type_nameRow10'));
$('#type_nameRow10').after().html('');
$('#typesRow10').append($(document.createElement('td')).attr('id', 'descriptionRow10'));
$('#descriptionRow10').after().html('');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow11'));
$('#typesRow11').append($(document.createElement('td')).attr('id', 'typeidRow11'));
$('#typeidRow11').after().html('');
$('#typesRow11').append($(document.createElement('td')).attr('id', 'type_nameRow11'));
$('#type_nameRow11').after().html('');
$('#typesRow11').append($(document.createElement('td')).attr('id', 'descriptionRow11'));
$('#descriptionRow11').after().html('');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow12'));
$('#typesRow12').append($(document.createElement('td')).attr('id', 'typeidRow12'));
$('#typeidRow12').after().html('Nome');
$('#typesRow12').append($(document.createElement('td')).attr('id', 'type_nameRow12'));
$('#type_nameRow12').after().html('asdf');
$('#typesRow12').append($(document.createElement('td')).attr('id', 'descriptionRow12'));
$('#descriptionRow12').after().html('a');
$('#types').append($(document.createElement('tr')).attr('id', 'typesRow13'));
$('#typesRow13').append($(document.createElement('td')).attr('id', 'typeidRow13'));
$('#typeidRow13').after().html('ab');
$('#typesRow13').append($(document.createElement('td')).attr('id', 'type_nameRow13'));
$('#type_nameRow13').after().html('c');
$('#typesRow13').append($(document.createElement('td')).attr('id', 'descriptionRow13'));
$('#descriptionRow13').after().html('d');
$('tr').attr('class', 'unselected');
var editingSession = false;
var newRow = false;
var newDone = false;
var displayMode = {false:'none', true:''};
var highlightedCell;
var tableName = types;
function newRowFun ( ) {
if(!newDone) { return true; }
newRow = false;
newDone = false;
return false;

}
function goUp(current) {
var obj = current.parent().prev().children('td:nth-child(' + (current.index() + 1) + ')');
if(obj.length == 0) {return;}
if(newRow)
newDone = true;
if(!newRowFun())
return;
return obj;
}
function goDown ( current) {
var obj = current.parent().next().children('td:nth-child(' + (current.index() + 1) + ')');
if(obj.length == 0) {return;}
if(newRow)
newDone = true;
if(!newRowFun())
return;
return obj;

}
function goLeft ( current) {
return current.prev('td');
}
function goRight ( current) {
return current.next('td');
}
function focusThis ( object) {
object.attr('tabindex', '0');
object.focus();

}
function stopEditing ( current) {
if(!editingSession) { return; }
if($('.highlighted').parent().hasClass('editing') && ! newRow && !($('.highlighted').hasClass('search'))) {
alert('update');
}
$('.highlighted').parent().removeClass('editing');
current.attr('contentEditable', 'false');
$('.borderColor').toggleClass('borderColor');
editingSession = false;

}
$('table').on('keydown', '', function (event) {
if (event.which == 39 || event.which == 38 || event.which == 37 || event.which == 40) {
if(editingSession) {return;}
event.preventDefault();
var current   = $('.highlighted');
var xs = {39:goRight, 37:goLeft, 40:goDown, 38:goUp };
var nextChild = xs[event.which](current);
var tempCurrent;
while(nextChild.parent().css('display') == 'none' && nextChild.length > 0) {
tempCurrent = nextChild;
nextChild = xs[event.which](tempCurrent);
if(nextChild == null) {return;}
}
if(nextChild.length == 0) {
return;
}
current.removeClass('highlighted');
nextChild.addClass('highlighted');

}

});
$('#types').on('keydown', '', function (event) {
if (event.which == 83 ) {
if (event.ctrlKey || event.metaKey) {
switch (String.fromCharCode(event.which).toLowerCase()) {
case 's':
event.preventDefault();
if(newRow) { newDone = true; }
if(!newRowFun()) { return; }
highlightedCell = $('.highlighted');
highlightedCell.toggleClass('highlighted');
stopEditing(highlightedCell);
$('#searchTable tr:nth-child(1)').find('td').eq(highlightedCell.index()).toggleClass('highlighted');
$('#searchTable').focus();
break;
}
}

}

});
$('body').on('keydown', '', function (event) {
if (event.which == 88 ) {
if (event.ctrlKey || event.metaKey) {
switch (String.fromCharCode(event.which).toLowerCase()) {
case 'x':
event.preventDefault();
$('.highlighted').parent().parent().parent().focus();
break;
}
}

}

});
$('#searchTable').on('keydown', '', function (event) {
if (event.which == 71 || event.which == 67) {
if (event.ctrlKey || event.metaKey) {
switch (String.fromCharCode(event.which).toLowerCase()) {
case 'g':
event.preventDefault();
var ups = highlightedCell;
while(ups.parent().css('display') == 'none' && ups.length>0) {
ups = goUp(ups);
}
if(ups.length == 0) {
ups = highlightedCell;
}
while(ups.parent().css('display') == 'none' && ups.length>0) {
ups = goDown(ups);
}
if (ups.length > 0) {
stopEditing($('.highlighted'));
$('.highlighted').blur();
$('.highlighted').toggleClass('highlighted');
highlightedCell = ups;
highlightedCell.toggleClass('highlighted');
highlightedCell.parent().parent().parent().focus();
}
break;
case 'c':
event.preventDefault();
$('.highlighted').after().html('');
break;
}
}

}

});
$('table').on('keydown', '', function (event) {
if (event.which == 114 ) {
event.preventDefault();
stopEditing($('.highlighted'));
child = $('.highlighted').parent().parent().children($('tr.unselected'));
child.each(function () {
if($(this).css('display') != 'none') {
$(this).toggleClass('unselected');
$(this).toggleClass('selected');
}
});

}

});
function startEditing ( ) {
if($('.highlighted').length == 0) {return;}
$('.highlighted').parent().addClass('editing');
$('.highlighted').attr('contentEditable', 'true');
$('.highlighted').addClass('borderColor');
editingSession = true;
$('.highlighted').focus();

}
$('table').on('keydown', function (event) {
if(event.which >= 65 && event.which <= 90 && !event.ctrlKey) {
startEditing();
}
});
$('table').on('keydown', '', function (event) {
if (event.which == 45 ) {
event.preventDefault();
startEditing();
}

});
$('table').on('keydown', '', function (event) {
if (event.which == 35 ) {
event.preventDefault();
$('.highlighted').parent().parent().parent().focus();
stopEditing($('.highlighted'));
}

});
$('table').on('keydown', '', function (event) {
if (event.which == 115 ) {
event.preventDefault();
child = $('.highlighted').parent();
child.toggleClass('unselected');
child.toggleClass('selected');

}

});
var nRowCount=14
$('#types').on('keypress', '', function (event) {
if (event.which == 13 ) {
allTrs = $(this).find('tr');
var fl = false;
if(allTrs.length > 1) {
allTrs.eq(allTrs.length-1).find('td').each(function () {
if($(this).text() != '') {fl = true;}
});
}
else {fl = true;}
if(fl) {
if(newRow) { newDone = true; }
if(!newRowFun()) { return; }
event.preventDefault();
$('#types').append($(document.createElement('tr')).attr('id', 'row' + nRowCount));
$('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
$('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
$('#row' + nRowCount).append($(document.createElement('td')).attr('id', 'col' + nRowCount))
$('#row' + nRowCount).attr('class','unselected');
$('#row'+nRowCount+' td:first').focus();
stopEditing($('.highlighted'));
$('#row' + nRowCount).attr('class','new');
newRow = true;
$('.highlighted').toggleClass('highlighted');
$('#row'+nRowCount+' td:first').toggleClass('highlighted');
nRowCount++
}

}

});
$('#searchTable').on('keyup', '', function (event) {
var nCols = $(this).parent().find('td').length;
var i;
$('#types tr').each(function() {
var flag = true;
for(i = 0; i < nCols; ++i) {
var textValue = (($('#searchTable tr:nth-child(1)').find('td').eq(i).text()));
if($(this).find('td').eq(i).text().toLowerCase().search(textValue.toLowerCase()) < 0 && $(this).find('td').length > 0) {
flag = false;
   break;
}
}
$(this).css('display', displayMode[flag]);
});

});

$('#types').focus();
$('#types').find('tr:nth-child(2)').find('td').eq(0).toggleClass('highlighted');});