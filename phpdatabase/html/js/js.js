$(document).ready(function () {
var editingSession = false;
$(document.body).attr('id',  'docBody');
$('#docBody').append($(document.createElement('div')).attr('id', 'prntDiv'));
$('#prntDiv').attr('class', 'container');
$('#docBody').append($(document.createElement('div')).attr('id', 'btnDiv'));
$('#btnDiv').attr('class', 'container');
$('#prntDiv').append($(document.createElement('table')).attr('id', 'searchTable'));
$('#searchTable').attr('align', 'center');
$('#searchTable').attr('tabindex', '1');
$('#searchTable').append($(document.createElement('tr')).attr('id', 'search-container-row'));
$('#search-container-row').append($(document.createElement('td')).attr('id', 'searchBox0'));
$('#searchBox0').attr('contenteditable', 'false');
$('#searchBox0').attr('class', 'search');
$('#prntDiv').append($(document.createElement('table')).attr('id', 'list'));
$('#list').attr('align', 'center');
$('#list').attr('tabindex', '0');
$('#list').append($(document.createElement('tr')).attr('id', 'prntDivHeading'));
$('#prntDivHeading').append($(document.createElement('th')).attr('id', 'tableNames'));
$('#tableNames').after().html('tableNames');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow0'));
$('#listRow0').append($(document.createElement('td')).attr('id', 'tableNamesRow0'));
$('#tableNamesRow0').after().html('types');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow1'));
$('#listRow1').append($(document.createElement('td')).attr('id', 'tableNamesRow1'));
$('#tableNamesRow1').after().html('typeinheritance');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow2'));
$('#listRow2').append($(document.createElement('td')).attr('id', 'tableNamesRow2'));
$('#tableNamesRow2').after().html('containerconcretetypes');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow3'));
$('#listRow3').append($(document.createElement('td')).attr('id', 'tableNamesRow3'));
$('#tableNamesRow3').after().html('boxwalls');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow4'));
$('#listRow4').append($(document.createElement('td')).attr('id', 'tableNamesRow4'));
$('#tableNamesRow4').after().html('validorientations');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow5'));
$('#listRow5').append($(document.createElement('td')).attr('id', 'tableNamesRow5'));
$('#tableNamesRow5').after().html('containerconcretetypefaces');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow6'));
$('#listRow6').append($(document.createElement('td')).attr('id', 'tableNamesRow6'));
$('#tableNamesRow6').after().html('containerconcretetypefacedoors');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow7'));
$('#listRow7').append($(document.createElement('td')).attr('id', 'tableNamesRow7'));
$('#tableNamesRow7').after().html('propertytypes');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow8'));
$('#listRow8').append($(document.createElement('td')).attr('id', 'tableNamesRow8'));
$('#tableNamesRow8').after().html('objects');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow9'));
$('#listRow9').append($(document.createElement('td')).attr('id', 'tableNamesRow9'));
$('#tableNamesRow9').after().html('objecttypes');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow10'));
$('#listRow10').append($(document.createElement('td')).attr('id', 'tableNamesRow10'));
$('#tableNamesRow10').after().html('properties');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow11'));
$('#listRow11').append($(document.createElement('td')).attr('id', 'tableNamesRow11'));
$('#tableNamesRow11').after().html('physicalentities');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow12'));
$('#listRow12').append($(document.createElement('td')).attr('id', 'tableNamesRow12'));
$('#tableNamesRow12').after().html('unit_list');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow13'));
$('#listRow13').append($(document.createElement('td')).attr('id', 'tableNamesRow13'));
$('#tableNamesRow13').after().html('measurementunits');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow14'));
$('#listRow14').append($(document.createElement('td')).attr('id', 'tableNamesRow14'));
$('#tableNamesRow14').after().html('containerconcretetypeproperties');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow15'));
$('#listRow15').append($(document.createElement('td')).attr('id', 'tableNamesRow15'));
$('#tableNamesRow15').after().html('containerobjects');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow16'));
$('#listRow16').append($(document.createElement('td')).attr('id', 'tableNamesRow16'));
$('#tableNamesRow16').after().html('childrenlist');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow17'));
$('#listRow17').append($(document.createElement('td')).attr('id', 'tableNamesRow17'));
$('#tableNamesRow17').after().html('noncontainerconcretetypes');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow18'));
$('#listRow18').append($(document.createElement('td')).attr('id', 'tableNamesRow18'));
$('#tableNamesRow18').after().html('compulsoryproperties');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow19'));
$('#listRow19').append($(document.createElement('td')).attr('id', 'tableNamesRow19'));
$('#tableNamesRow19').after().html('noncontainerobjects');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow20'));
$('#listRow20').append($(document.createElement('td')).attr('id', 'tableNamesRow20'));
$('#tableNamesRow20').after().html('noncontainerobjectproperties');
$('#list').append($(document.createElement('tr')).attr('id', 'listRow21'));
$('#listRow21').append($(document.createElement('td')).attr('id', 'tableNamesRow21'));
$('#tableNamesRow21').after().html('immutableproperties');
$('tr').attr('class', 'unselected');
var editingSession = false;
var displayMode = {false:'none', true:''};
var highlightedCell;
var currentVal;
var tableName = 'list';
var columns = ['typeid', 'type_name', 'description'];
var tableCols = {'types':"['typeid', 'type_name', 'description']", 'typeinheritance':"['typeid', 'super_typeid']", 'containerconcretetypes':"['containerconcretetypeid', 'containerlengthab', 'containerlengthad', 'containerlengthae', 'containerthickness', 'userobjectcommentglobal']", 'boxwalls':"['boxwallsidename']", 'validorientations':"['boxvalidwallorientations']", 'containerconcretetypefaces':"['containerconcretewallid', 'containerconcretetypeid', 'containersides', 'wallorientation']", 'containerconcretetypefacedoors':"['containerconcretedoorid', 'containerconcretewallid', 'doorwidthab', 'doorlengthad', 'doordisplacementab', 'doordisplacementad', 'dooralignmentab', 'dooralignmentad']", 'propertytypes':"['propertytype', 'physicalentityid', 'userdescription']", 'objects':"['objectid', 'objecttype']", 'objecttypes':"['objecttype']", 'properties':"['propertyid', 'propertytype', 'userdescription']", 'physicalentities':"['physicalentityid', 'entity_name']", 'unit_list':"['unit_id', 'unit_name', 'unit_abbr', 'physicalentityid', 'multiplication_factor']", 'measurementunits':"['measurementunitid', 'measurementunittype']", 'containerconcretetypeproperties':"['containerconcretetypeid', 'propertyid', 'propertyvalue', 'measurementunits', 'usercommentglobal']", 'containerobjects':"['objectid', 'containerconcretetypeid', 'userlabel', 'userobjectcommentlocal']", 'childrenlist':"['parentobjectid', 'objectid', 'displacementab', 'displacementad', 'displacementae', 'childfrontfacingparent', 'childtopfacingparent']", 'noncontainerconcretetypes':"['noncontainerconcretetypeid', 'noncontainerlengthab', 'noncontainerlengthad', 'noncontainerlengthae', 'userobjectcommentglobal']", 'compulsoryproperties':"['propertyid']", 'noncontainerobjects':"['objectid', 'noncontainerconcretetypeid', 'userlabel', 'userobjectcommentlocal']", 'noncontainerobjectproperties':"['objectid', 'userassignedid', 'propertyid', 'propertyvalue', 'usercommentlocal', 'userlabel']", 'immutableproperties':"['propertyid']"}
 var c = function() {
return({
log: function(msg) {
consoleDiv = document.getElementById('btnDiv');
para = document.createElement('p');
text = document.createTextNode(msg);
para.appendChild(text);
consoleDiv.appendChild(para);
}
});
}();
function newRowFun ( current) {
if(!current.hasClass('new')) { return true; }
var vals = [];
current.find('td').each(function () {
vals.push($(this).text());
});
var query = '"insert into ' + tableName + ' (';
for(i = 0; i < columns.length-1; ++i) {
query += columns[i] + ', ';
};
query += columns[columns.length-1] + ') values (';
for(i = 0; i < vals.length-1; ++i) {
query += '\'' + vals[i] + '\', ';
}
query += '\'' + vals[i] + '\')"';
$.ajax({ url: '2.php',
data: {'callFun': query},
type: 'post',
success: function(output) {
if (output == 0) { current.removeClass('new error'); c.log(query + ': succeeded.');}
else {current.addClass('error');  c.log(query + ': failed.');}
}
});return true;
}
function goUp(current) {
var obj = current.parent().prev().children('td:nth-child(' + (current.index() + 1) + ')');
if(obj.length == 0) {return;}
if(!newRowFun($('.highlighted').parent()))
return;
return obj;
}
function goDown ( current) {
var obj = current.parent().next().children('td:nth-child(' + (current.index() + 1) + ')');
if(obj.length == 0) {return;}
if(!newRowFun($('.highlighted').parent()))
return;
return obj;

}
function goLeft ( current) {
return current.prev('td');
}
function goRight ( current) {
return current.next('td');
}
function focusThis ( object) {
object.attr('tabindex', '0');
object.focus();

}
function stopEditing ( current) {
if(!editingSession) { return; }
if(current.parent().hasClass('editing') && !(current.parent().hasClass('new')) && !(current.hasClass('search'))) {
var vals = [];
current.parent().find('td').each(function () {
vals.push($(this).text());
});
var query = "\"update " + tableName + " set " + columns[current.index()] + "= '" + current.text() + "' where ";
for (i = 0; i < vals.length-1; ++i) {
if(i == current.index())
query += columns[i] + " = '" + currentVal + "' AND ";
else
query += columns[i] + " = '" + vals[i] + "' AND ";
}
if(current.index() == vals.length-1) {
query += columns[vals.length-1] + " = '" + currentVal + "' \"";
}
else {
query += columns[vals.length-1] + " = '" + vals[vals.length-1] + "' \"";
}
$.ajax({ url: '2.php',
data: {'callFun': query},
type: 'post',
success: function(output) {
if (output == 0) { current.removeClass('error'); c.log(query + ': succeeded.');}
else {current.addClass('error');  c.log(query + ': failed.');}
}
});}
current.parent().removeClass('editing');
current.attr('contentEditable', 'false');
$('.borderColor').toggleClass('borderColor');
editingSession = false;

}
$('table').on('keydown', '', function (event) {
if (event.which == 39 || event.which == 38 || event.which == 37 || event.which == 40) {
if(editingSession) {return;}
event.preventDefault();
var current   = $('.highlighted');
var xs = {39:goRight, 37:goLeft, 40:goDown, 38:goUp };
var nextChild = xs[event.which](current);
if(nextChild == null) {return;}
var tempCurrent;
while(nextChild.parent().css('display') == 'none' && nextChild.length > 0) {
tempCurrent = nextChild;
nextChild = xs[event.which](tempCurrent);
if(nextChild == null) {return;}
}
if(nextChild.length == 0) {
return;
}
current.removeClass('highlighted');
nextChild.addClass('highlighted');

}

});
$('#list').on('keydown', '', function (event) {
if (event.which == 83 ) {
if (event.ctrlKey || event.metaKey) {
switch (String.fromCharCode(event.which).toLowerCase()) {
case 's':
event.preventDefault();
if(!newRowFun($('.highlighted').parent())) { return; }
highlightedCell = $('.highlighted');
highlightedCell.toggleClass('highlighted');
stopEditing(highlightedCell);
$('#searchTable tr:nth-child(1)').find('td').eq(highlightedCell.index()).toggleClass('highlighted');
$('#searchTable').focus();
break;
}
}

}

});
$('body').on('keydown', '', function (event) {
if (event.which == 88 ) {
if (event.ctrlKey || event.metaKey) {
switch (String.fromCharCode(event.which).toLowerCase()) {
case 'x':
event.preventDefault();
$('.highlighted').parent().parent().parent().focus();
break;
}
}

}

});
$('body').on('keydown', '', function (event) {
if (event.which == 27 ) {
$.ajax({ url: '1.php',
data: {'callFun':  "python newUi.py list 10 \"['typeid', 'type_name', 'description']\" \"dbname='se1' user='postgres' host='127.0.0.1' password='1'\""},
type: 'post',
success: function(output) {
window.location.replace('3.php');
}});

}

});
$('#searchTable').on('keydown', '', function (event) {
if (event.which == 71 || event.which == 67) {
if (event.ctrlKey || event.metaKey) {
switch (String.fromCharCode(event.which).toLowerCase()) {
case 'g':
event.preventDefault();
var ups = highlightedCell;
if (ups!= null) {
while(ups.parent().css('display') == 'none') {
ups = goUp(ups);
if(ups == null) { break; }
}
}
if(ups == null) {
ups = highlightedCell;
}
while(ups != null && ups.parent().css('display') == 'none' ) {
ups = goDown(ups);
if(ups == null) { break; }
}
if (ups != null) {
stopEditing($('.highlighted'));
$('.highlighted').blur();
$('.highlighted').toggleClass('highlighted');
highlightedCell = ups;
highlightedCell.toggleClass('highlighted');
highlightedCell.parent().parent().parent().focus();
}
break;
case 'c':
event.preventDefault();
$('.highlighted').after().html('');
break;
}
}

}

});
$('table').on('keydown', '', function (event) {
if (event.which == 114 ) {
event.preventDefault();
stopEditing($('.highlighted'));
child = $('.highlighted').parent().parent().children($('tr.unselected'));
child.each(function () {
if($(this).css('display') != 'none') {
$(this).toggleClass('unselected');
$(this).toggleClass('selected');
}
});

}

});
function startEditing ( ) {
if($('.highlighted').length == 0) {return;}
$('.highlighted').parent().addClass('editing');
$('.highlighted').attr('contentEditable', 'true');
$('.highlighted').addClass('borderColor');
editingSession = true;
$('.highlighted').focus();
currentVal = $('.highlighted').text();

}
$('table').on('keydown', function (event) {
if(event.which >= 65 && event.which <= 90 && !event.ctrlKey) {
startEditing();
}
});
$('table').on('keydown', '', function (event) {
if (event.which == 45 ) {
event.preventDefault();
startEditing();
}

});
$('table').on('keydown', '', function (event) {
if (event.which == 35 ) {
event.preventDefault();
$('.highlighted').parent().parent().parent().focus();
stopEditing($('.highlighted'));
}

});
$('table').on('keydown', '', function (event) {
if (event.which == 115 ) {
event.preventDefault();
child = $('.highlighted').parent();
child.toggleClass('unselected');
child.toggleClass('selected');

}

});
var nRowCount=22
function getColList ( tableName) {
return tableCols[tableName];

}
$('#list').on('keypress', '', function (event) {
if (event.which == 13 ) {
command = 'python newUi.py ' + $('.highlighted').text() + ' 0 "' + getColList($('.highlighted').text()) + '" "dbname=\'se1\' user=\'postgres\' host=\'127.0.0.1\' password=\'1\'"';
$.ajax({ url: '1.php',
data: {'callFun': command},
type: 'post',
success: function(output) {
window.location.replace('3.php');
}});

}

});
$('#searchTable').on('keyup', '', function (event) {
var nCols = $(this).parent().find('td').length;
var i;
$('#list tr').each(function() {
var flag = true;
for(i = 0; i < nCols; ++i) {
var textValue = (($('#searchTable tr:nth-child(1)').find('td').eq(i).text()));
if($(this).find('td').eq(i).text().toLowerCase().search(textValue.toLowerCase()) < 0 && $(this).find('td').length > 0) {
flag = false;
   break;
}
}
$(this).css('display', displayMode[flag]);
});

});

$('#list').focus();
$('#list').find('tr:nth-child(2)').find('td').eq(0).toggleClass('highlighted');});