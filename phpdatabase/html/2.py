import sys, getopt
import ast
import psycopg2

def main(argv):
    
    try:
        conn = psycopg2.connect("dbname='se1' user='postgres' host='127.0.0.1' password='1'")
    except:
        print "Can't open database"
        sys.exit(2);
    
    curDB = conn.cursor()
   
    try:
        sSqlQuery = argv[0]
        curDB.execute(sSqlQuery)
        conn.commit();
    except:
        print "Error: No Columns found in the Tables.\n"
        sys.exit(1)
    sys.exit(0)
    
if __name__ == "__main__":
    main(sys.argv[1:])

